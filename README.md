# DkML

This project has moved. Please update your bookmarks to https://gitlab.com/dkml/distributions/dkml.

## Status

This (original) project has packages in its Package Registry that support DkML 1.2.0. When DkML 1.2.0 is EOL at the end of 2023, this project will be removed.
